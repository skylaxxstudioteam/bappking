package com.skylaxx.bappking;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by Skylaxx Studio on 01/09/2017.
 */

@RunWith(AndroidJUnit4.class)
public class CorrectRecipeStepList {
    public static final String RECIPE_NUTELLA_NAME="Nutella Pie";
    @Rule
    public ActivityTestRule<RecipesActivity> mActivityTestRule= new ActivityTestRule<RecipesActivity>(RecipesActivity.class);

    @Test
    public void ClickNutellaPieRecipe_ShowNutellaActivity()
    {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        onView(withId(R.id.rv_recipes_list))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        onView(withText(RECIPE_NUTELLA_NAME)).check(matches(withParent(withId(R.id.toolbar))));


    }
}
