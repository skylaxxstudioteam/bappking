package com.skylaxx.bappking;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.anything;

/**
 * Created by Jose Sanchez on 31/08/17.
 */

@RunWith(AndroidJUnit4.class)
public class RecipesActivityItemClick {

    public static final String RECIPE_NUTELLA_NAME="Nutella Pie";


    @Rule
    public ActivityTestRule<RecipesActivity> mActivityTestRule= new ActivityTestRule<RecipesActivity>(RecipesActivity.class);

    //This test checks the step list for the first recipe and the ingredient list beign displayed
    @Test
public void ClickRecipe_ShowIngredientsAndSteps()
{
    try {
        Thread.sleep(2000);
    } catch (InterruptedException e) {
        e.printStackTrace();
    }

    onView(withId(R.id.rv_recipes_list))
            .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

    onView(withId(R.id.tv_ingredients)).check(matches(isDisplayed()));
    onView(withId(R.id.recipestep_list)).check(matches(isDisplayed()));

}
}
