package com.skylaxx.bappking;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;


public class RecipeStepDetailActivity extends AppCompatActivity {

    public static final String TAG="StepDetailActivity";
    public boolean isLanded=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            this.requestWindowFeature(Window.FEATURE_NO_TITLE); //Remove title bar
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); //Remove notification bar
        }

        setContentView(R.layout.activity_recipestep_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        if(toolbar!=null) {
            toolbar.setTitle(RecipesActivity.recipeSelected.getName());
            setSupportActionBar(toolbar);
        }


        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);

        }

        if (savedInstanceState == null) {
            Log.i(TAG,"La savedInatace es nulla, se crea y agrega el fragment");
            Bundle arguments = new Bundle();
            arguments.putInt(RecipeStepDetailFragment.STEP_INDEX_PARAM,
                    getIntent().getIntExtra(RecipeStepDetailFragment.STEP_INDEX_PARAM,0));
            RecipeStepDetailFragment fragment = new RecipeStepDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.recipestep_detail_container, fragment)
                    .commit();
        }


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            NavUtils.navigateUpTo(this, new Intent(this, RecipeStepListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



}
