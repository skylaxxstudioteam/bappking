package com.skylaxx.bappking;

import android.app.Activity;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.net.URLConnection;

public class RecipeStepDetailFragment extends Fragment {

    public static final String STEP_INDEX_PARAM="index";
    public static final String VIDEO_POSITION_PARAM="seekTo";
    public static final String TAG="tepDetailFragment";
    private TextView stepDescripction;
    private int stepIndex;
    Button previousStep;
    Button nextStep;
    SimpleExoPlayer mExoPlayer;
    SimpleExoPlayerView mExoPlayerView;
    TextView tvNoVideo;
    ImageView thumbnail;
    long playerPosition;


    public RecipeStepDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState==null){
            stepIndex=getArguments().getInt(STEP_INDEX_PARAM);

            Log.i(TAG,"stepIndexRecuperado de:Argunmentos");
        }else
        {
            stepIndex=savedInstanceState.getInt(STEP_INDEX_PARAM);
            playerPosition=savedInstanceState.getLong(VIDEO_POSITION_PARAM);
            Log.i(TAG,"stepIndexRecuperado de: SAVED INSTANCE");
        }

        Log.i("fragmentDetail","se recibio int extra poso:"+stepIndex);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(STEP_INDEX_PARAM,stepIndex);
        if(mExoPlayer!=null)
        {
            outState.putLong(VIDEO_POSITION_PARAM,mExoPlayer.getCurrentPosition());
        }

        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.recipestep_detail, container, false);

        stepDescripction= (TextView) rootView.findViewById(R.id.tv_step_description);
        previousStep= (Button) rootView.findViewById(R.id.bt_previous);
        nextStep= (Button) rootView.findViewById(R.id.bt_next);
        mExoPlayerView= (SimpleExoPlayerView) rootView.findViewById(R.id.epv_mediaplayer);
        tvNoVideo=(TextView)rootView.findViewById(R.id.no_video);
        thumbnail= (ImageView) rootView.findViewById(R.id.thumbnail);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(previousStep!=null)
        previousStep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stepIndex--;
                if (stepIndex>=0)
                {
                    updateDisplay();
                }

            }
        });

        if(nextStep!=null)
        nextStep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                stepIndex++;
                if(stepIndex<RecipesActivity.recipeSelected.getSteps().size())
                {
                    updateDisplay();
                }else
                {
                    if(stepIndex>=RecipesActivity.recipeSelected.getSteps().size()-1)
                    {
                        nextStep.setEnabled(false);
                    }else
                    {
                        nextStep.setEnabled(true);
                    }
                }

            }
        });

        updateDisplay();

    }

    public boolean hasValidVideo() {
        return RecipesActivity.recipeSelected.getSteps().get(stepIndex).getVideoURL()!=null &&
                !RecipesActivity.recipeSelected.getSteps().get(stepIndex).getVideoURL().isEmpty();
    }
    public boolean hasValidThumbnail()
    {
        String thumnail=RecipesActivity.recipeSelected.getSteps().get(stepIndex).getThumbnailURL();
        if(thumnail!=null && !thumnail.isEmpty())
        {
            String type=URLConnection.guessContentTypeFromName(thumnail);
            Log.i(TAG,"TIPO THUMB:"+type);
            return type.contains("image");
        }

        return false;



    }
    public void updateDisplay()
    {
        releasePlayer();
        if(previousStep!=null && nextStep!=null) {
            if (stepIndex == 0) {
                previousStep.setEnabled(false);
            } else {
                previousStep.setEnabled(true);
            }
            if (stepIndex >= RecipesActivity.recipeSelected.getSteps().size() - 1) {
                nextStep.setEnabled(false);
            } else {
                nextStep.setEnabled(true);
            }
        }

        if(stepDescripction!=null)
        stepDescripction.setText(RecipesActivity.recipeSelected.getSteps().get(stepIndex).getDescription());

        Log.i(TAG,"VIDEO URL:"+RecipesActivity.recipeSelected.getSteps().get(stepIndex).getVideoURL());
        if(hasValidVideo())
        {
            initializePlayer(Uri.parse(RecipesActivity.recipeSelected.getSteps().get(stepIndex).getVideoURL()));
        }else if(hasValidThumbnail()) {
            if(thumbnail!=null);
        }else
        {
            showNoVideoLegend();
        }

    }
    public void showThumnail()
    {

        if(thumbnail!=null)
        {
            Picasso.with(getContext()).load(RecipesActivity.recipeSelected.getSteps().get(stepIndex).getThumbnailURL()).into(thumbnail);
            if(mExoPlayerView!=null)
                mExoPlayerView.setVisibility(View.INVISIBLE);
            if(tvNoVideo!=null)
                tvNoVideo.setVisibility(View.INVISIBLE);
            thumbnail.setVisibility(View.VISIBLE);
        }

    }

    public void showNoVideoLegend()
    {
        if(mExoPlayerView!=null)
        mExoPlayerView.setVisibility(View.INVISIBLE);
        if(tvNoVideo!=null)
            tvNoVideo.setVisibility(View.VISIBLE);
        if(thumbnail!=null)
            thumbnail.setVisibility(View.INVISIBLE);
    }
    public void hideNoVideoLegend()
    {
        if(mExoPlayerView!=null)
            mExoPlayerView.setVisibility(View.VISIBLE);
        if(tvNoVideo!=null)
            tvNoVideo.setVisibility(View.INVISIBLE);
        if(thumbnail!=null)
            thumbnail.setVisibility(View.INVISIBLE);
    }
    public void initializePlayer(Uri mediaUri)
    {
        hideNoVideoLegend();
        if(mExoPlayer==null)
        {

            TrackSelector trackSelector= new DefaultTrackSelector();
            LoadControl loadControl= new DefaultLoadControl();
            mExoPlayer= ExoPlayerFactory.newSimpleInstance(getContext(),trackSelector);
            mExoPlayerView.setPlayer(mExoPlayer);
            String userAgent = Util.getUserAgent(getContext(), "Bappking");
            MediaSource mediaSource = new ExtractorMediaSource(mediaUri, new DefaultDataSourceFactory(
                    getContext(), userAgent), new DefaultExtractorsFactory(), null, null);
            if(playerPosition!=0)
            {
                mExoPlayer.seekTo(playerPosition);
            }
            mExoPlayer.prepare(mediaSource);
            mExoPlayer.setPlayWhenReady(true);
        }
    }
    public void releasePlayer()
    {
        if(mExoPlayer!=null)
        {
            mExoPlayer.stop();
            mExoPlayer.release();
            mExoPlayer=null;
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        releasePlayer();
    }

    @Override
    public void onPause() {
        super.onPause();
        releasePlayer();
    }

    @Override
    public void onStop() {
        super.onStop();
        releasePlayer();
    }
}
