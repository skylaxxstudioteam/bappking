package com.skylaxx.bappking;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.skylaxx.bappking.model.Ingredient;
import com.skylaxx.bappking.model.Recipe;
import com.skylaxx.bappking.model.Step;

import java.util.List;

public class RecipeStepListActivity extends AppCompatActivity {
    public static final String TAG="RecipeStepListActivity";
    private boolean mTwoPane;
    TextView tvIngerdientes;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipestep_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.m_toolbar);
        if(toolbar!=null) {
            toolbar.setTitle(RecipesActivity.recipeSelected.getName());
            setSupportActionBar(toolbar);
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);

        }
        Log.i(TAG,"receta seleccionada:"+RecipesActivity.recipeSelected.getName());
//        toolbar.setTitle(RecipesActivity.recipeSelected.getName());
//        setTitle(RecipesActivity.recipeSelected.getName());

        recyclerView = (RecyclerView) findViewById(R.id.recipestep_list);
        assert recyclerView != null;
        setupRecyclerView( recyclerView);

        tvIngerdientes= (TextView) findViewById(R.id.tv_ingredients);
        if(tvIngerdientes!=null)
        tvIngerdientes.setText(RecipesActivity.recipeSelected.getIngredientsFoDisplay());

        if (findViewById(R.id.recipestep_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(RecipesActivity.recipeSelected.getSteps()));
    }

    public class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private final List<Step> mValues;

        public SimpleItemRecyclerViewAdapter(List<Step> items) {
            mValues = items;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recipestep_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.mItem = mValues.get(position);
            holder.mIdView.setText(String.valueOf(mValues.get(position).getId()+1));
            holder.mContentView.setText(mValues.get(position).getShortDescription());

            if (mTwoPane && position==0) {
                holder.mView.setSelected(true);
                Bundle arguments = new Bundle();
                arguments.putInt(RecipeStepDetailFragment.STEP_INDEX_PARAM, position);
                RecipeStepDetailFragment fragment = new RecipeStepDetailFragment();
                fragment.setArguments(arguments);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.recipestep_detail_container, fragment)
                        .commit();
            }

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mTwoPane) {
                        Bundle arguments = new Bundle();
                        arguments.putInt(RecipeStepDetailFragment.STEP_INDEX_PARAM, position);
                        RecipeStepDetailFragment fragment = new RecipeStepDetailFragment();
                        fragment.setArguments(arguments);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.recipestep_detail_container, fragment)
                                .commit();
                    } else {
                        Context context = v.getContext();
                        Intent intent = new Intent(context, RecipeStepDetailActivity.class);
                        intent.putExtra(RecipeStepDetailFragment.STEP_INDEX_PARAM, position);
                        Log.i(TAG,"Enviando extra posicion:"+position);
                        context.startActivity(intent);
                    }
                }
            });
        }


        @Override
        public int getItemCount() {
            return mValues.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final TextView mIdView;
            public final TextView mContentView;
            public Step mItem;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mIdView = (TextView) view.findViewById(R.id.id);
                mContentView = (TextView) view.findViewById(R.id.content);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mContentView.getText() + "'";
            }
        }
    }
}
