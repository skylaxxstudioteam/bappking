package com.skylaxx.bappking;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.skylaxx.bappking.model.Recipe;
import com.skylaxx.bappking.utils.RecipesService;
import com.skylaxx.bappking.widget.IngredientsWidgetProvider;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RecipesActivity extends AppCompatActivity implements RecipesAdapter.SelectRecipeListener {
    public static final String TAG="RecipesActivity";
    private RecyclerView rvRecipesList;
    private RecipesAdapter recipesAdapter;
    public static Recipe recipeSelected;
    public List<Recipe> recipes;
    ProgressBar loadinRecipes;

    public static final String INGREDIENTS_KEY="ingedientsList";
    public static final String INGREDIENTS_RECIPE_NAME="ingedientsRecipeName";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipes);

        loadinRecipes= (ProgressBar) findViewById(R.id.lp_loading_repices);

        rvRecipesList= (RecyclerView) findViewById(R.id.rv_recipes_list);
        // BEGIN **************************** retrofit implementatiton

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://d17h27t6h515a5.cloudfront.net")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            RecipesService service = retrofit.create(RecipesService.class);
            Call<List<Recipe>> recipesCall = service.getAllRecipes();
            recipes = new ArrayList<>();
            showLoading();
            recipesCall.enqueue(new Callback<List<Recipe>>() {
                @Override
                public void onResponse(Call<List<Recipe>> call, Response<List<Recipe>> response) {
                    List<Recipe> recipes = response.body();
                    setRecipiesToRecyclerView(recipes);
                    Log.i(TAG, "Se obtuvo respuesta de recipes: " + recipes.size());
                }

                @Override
                public void onFailure(Call<List<Recipe>> call, Throwable t) {
                    Log.e(TAG, "Error con la respuesta: :(" + t);
                }
            });


        // END **************************** retrofit implementatiton
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            GridLayoutManager layoutMode=new GridLayoutManager(this,3);
            rvRecipesList.setLayoutManager(layoutMode);
        }else
        {
            LinearLayoutManager layoutMode = new LinearLayoutManager(this);
            layoutMode.setOrientation(LinearLayoutManager.VERTICAL);
            rvRecipesList.setLayoutManager(layoutMode);
        }

        rvRecipesList.setAdapter(recipesAdapter);
    }

    public void setRecipiesToRecyclerView(List<Recipe> data)
    {
        hideLoading();
        recipes=data;
        recipesAdapter= new RecipesAdapter(this,data,this);
        rvRecipesList.setAdapter(recipesAdapter);

    }

    public void saveIngredrients(int recipeIndex)
    {
        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(this.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(recipes.get(recipeIndex).getIngredients());
        prefsEditor.putString(INGREDIENTS_KEY, json);
        prefsEditor.putString(INGREDIENTS_RECIPE_NAME,recipes.get(recipeIndex).getName());
        prefsEditor.commit();
        Log.d("TAG","SAVING.....ingredientes json = " + json);
        Log.d("TAG","SAVING.....recipe name = " + recipes.get(recipeIndex).getName());
    }



    @Override
    public void OnSelectRecipie(int position) {
        saveIngredrients(position);
        updateWidgetWithNewIngredientes();
        recipeSelected=recipes.get(position);
        Intent toRecipeSteps= new Intent(this,RecipeStepListActivity.class);
        startActivity(toRecipeSteps);
    }

    public void updateWidgetWithNewIngredientes()
    {
        try {
            Intent updateWidget = new Intent(this, IngredientsWidgetProvider.class);
            updateWidget.setAction(IngredientsWidgetProvider.UPDATE_INGREDIENTS_LIST);
            PendingIntent pending = PendingIntent.getBroadcast(this, 0, updateWidget, PendingIntent.FLAG_CANCEL_CURRENT);
            pending.send();
        } catch (PendingIntent.CanceledException e) {
            Log.e("UpdateWidget","Error widgetTrial()="+e.toString());
        }
    }

    public void showLoading()
    {
        if(loadinRecipes!=null)
        {
            loadinRecipes.setVisibility(View.VISIBLE);
        }
    }
    public void hideLoading()
    {
        if(loadinRecipes!=null)
        {
            loadinRecipes.setVisibility(View.INVISIBLE);
        }
    }
}
