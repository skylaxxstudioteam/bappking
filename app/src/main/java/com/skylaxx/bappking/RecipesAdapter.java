package com.skylaxx.bappking;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.skylaxx.bappking.model.Recipe;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by Jose Sanchez on 25/08/17.
 */

public class RecipesAdapter  extends RecyclerView.Adapter<RecipesAdapter.ViewHolder>{

    public static final String TAG="RecipesAdapter";
    private  List<Recipe> recipes;
    private SelectRecipeListener mListener;
    private Context context;

    public RecipesAdapter(Context context, List<Recipe> recipes, SelectRecipeListener listener)
    {
        mListener=listener;
        Log.i(TAG,"creando adaptador:"+recipes);
        this.recipes=recipes;
        this.context=context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.recipe_item,parent,false);
        ViewHolder holder= new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
            holder.bind(recipes.get(position).getName(),recipes.get(position).getServings(),recipes.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return recipes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {

        public TextView recipeTitle;
        public TextView recipeServings;
        public ImageView recipeImage;

        public ViewHolder(View itemView) {
            super(itemView);
            recipeTitle= (TextView) itemView.findViewById(R.id.tv_recipe_title);
            recipeServings= (TextView) itemView.findViewById(R.id.tv_servings);
            recipeImage= (ImageView) itemView.findViewById(R.id.recipe_image);
            itemView.setOnClickListener(this);
        }

        public void bind(String titulo, int servings,String imageUrl)
        {
            recipeTitle.setText(titulo);
            recipeServings.setText(String.valueOf(servings)+" servings");
            if(imageUrl!=null && !imageUrl.isEmpty())
            {
                Picasso.with(context).load(imageUrl).placeholder(R.drawable.recipe_placeholder).into(recipeImage);
            }

        }

        @Override
        public void onClick(View view) {
            int pos=getAdapterPosition();
            mListener.OnSelectRecipie(pos);
        }
    }

    public interface SelectRecipeListener
    {
        void OnSelectRecipie(int position);
    }
}
