package com.skylaxx.bappking.utils;

import com.skylaxx.bappking.model.Recipe;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Jose Sanchez on 25/08/17.
 */

public interface RecipesService {

    @GET("/topher/2017/May/59121517_baking/baking.json")
    Call<List<Recipe>> getAllRecipes();
}
