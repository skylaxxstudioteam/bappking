package com.skylaxx.bappking.widget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.RemoteViews;

import com.skylaxx.bappking.R;
import com.skylaxx.bappking.RecipesActivity;

/**
 * Implementation of App Widget functionality.
 */
public class IngredientsWidgetProvider extends AppWidgetProvider {

    public static final String UPDATE_INGREDIENTS_LIST="updateListWidget";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

        for(int id:appWidgetIds)
        {
            RemoteViews remoteViews = updateWidgetListView(context,id);
            remoteViews.setTextViewText(R.id.tv_recipe_name,loadRecipeName(context));
            appWidgetManager.updateAppWidget(id,remoteViews);
            appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds,R.id.listViewWidget);
        }

        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    static RemoteViews updateWidgetListView(Context context,int widgetId)
    {

        RemoteViews remoteViews = new RemoteViews(
                context.getPackageName(),R.layout.ingredients_widget_provider);

        Intent svcIntent = new Intent(context, IngredientsWidgetService.class);
        svcIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
        svcIntent.setData(Uri.parse(svcIntent.toUri(Intent.URI_INTENT_SCHEME)));

        remoteViews.setRemoteAdapter(R.id.listViewWidget,
                svcIntent);

        remoteViews.setEmptyView(R.id.listViewWidget, R.id.empty_view);
        return remoteViews;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        Log.v("WidgetProvider", "se recibio peticion con accion:" + intent.getAction());
        if (intent.getAction().equals(UPDATE_INGREDIENTS_LIST)) {
            ComponentName provider = new ComponentName(context, IngredientsWidgetProvider.class);
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            int[] appWidgetIds = appWidgetManager.getAppWidgetIds(provider);
            for(int id:appWidgetIds)
            {
                RemoteViews remoteViews = updateWidgetListView(context,id);
                remoteViews.setTextViewText(R.id.tv_recipe_name,loadRecipeName(context));
                appWidgetManager.updateAppWidget(id,remoteViews);
                appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds,R.id.listViewWidget);

            }
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    public static String loadRecipeName(Context context)
    {

        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        String name = appSharedPrefs.getString(RecipesActivity.INGREDIENTS_RECIPE_NAME, "");
        Log.i("ListFactory","LOAD complete  receta:"+name);
        return  name;
    }
}

