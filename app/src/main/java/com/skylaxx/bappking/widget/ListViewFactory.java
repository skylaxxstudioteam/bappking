package com.skylaxx.bappking.widget;

import android.app.LauncherActivity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.skylaxx.bappking.R;
import com.skylaxx.bappking.RecipesActivity;
import com.skylaxx.bappking.model.Ingredient;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jose Sanchez on 30/08/17.
 */

public class ListViewFactory implements RemoteViewsService.RemoteViewsFactory {

    private List<Ingredient> ingredientes = new ArrayList<>();
    private Context context = null;
    private int appWidgetId;
    private String recipeName;

    public ListViewFactory(Context context, Intent intent) {
        this.context = context;
        appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);

    }

    public List<Ingredient> loadIngredients()
    {
        Gson gson = new Gson();
        Type type = new TypeToken<List<Ingredient>>(){}.getType();
        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        String json = appSharedPrefs.getString(RecipesActivity.INGREDIENTS_KEY, "");
        List<Ingredient> ingredientes = gson.fromJson(json, type);
        Log.i("ListFactory","LOAD complete  lista ingredientes:"+ingredientes);
        return ingredientes;
    }


    @Override
    public void onCreate() {

    }

    @Override
    public void onDataSetChanged() {
        ingredientes=loadIngredients();
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public int getCount() {
        return ingredientes.size();
    }

    @Override
    public RemoteViews getViewAt(int position) {
        final RemoteViews remoteView = new RemoteViews(
                context.getPackageName(), R.layout.widget_list_item);

        remoteView.setTextViewText(R.id.tv_recipe_name,recipeName);
        Ingredient ingredient = ingredientes.get(position);
        remoteView.setTextViewText(R.id.cantidad, String.valueOf(ingredient.getQuantity()));
        remoteView.setTextViewText(R.id.unidad, ingredient.getMeasure());
        remoteView.setTextViewText(R.id.ingredient, ingredient.getIngredient());


        return remoteView;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }
}
